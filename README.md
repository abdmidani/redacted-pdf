## Redacted PDF
Live: https://redacted-pdf.netlify.app/

## Description
The goal is to allow users to download a copy of a PDF document with all annotation locations redacted.

## Technologies
Latest VueJS Version.

## Security
The redacted text is protected and cannot be stolen using inspect element or any tools, also after you save the file.

## Installation
Installing Dependencies: `npm i`
Run The Project on Localhost: `npm run serve`
Build For Production: `npm build`

## Usage
Step 1:

![Select the text then press the last one on the right ](image.png)

Step 2:

![Press on the selected texts then press apply as you can see in the screenshot](image-1.png)